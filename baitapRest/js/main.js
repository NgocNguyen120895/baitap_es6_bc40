
function tinhDTB(output, ...arg) {
    let result = 0;
    let len = arg.length;

    arg.forEach((arg) => {
        result += arg / len;
    });
    console.log(len)
    return document.getElementById(output).innerHTML = result
}

document.getElementById('btnKhoi1').onclick = () => {
    var t = document.getElementById('inpToan').value * 1,
        l = document.getElementById('inpLy').value * 1,
        h = document.getElementById('inpHoa').value * 1;
    tinhDTB("tbKhoi1", t, l, h);
}

document.getElementById('btnKhoi2').onclick = () => {
    var v = document.getElementById('inpVan').value * 1,
        s = document.getElementById('inpSu').value * 1,
        d = document.getElementById('inpDia').value * 1,
        e = document.getElementById('inpEnglish').value * 1;

    tinhDTB("tbKhoi2", v, s, d, e);
}