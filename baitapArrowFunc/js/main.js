
const colorList = ["pallet", "viridian", "pewter", "cerulean",
    "vermillion", "lavender", "celadon", "saffron", "fuschia", "cinnabar"];

loadColor = (arr) => {
    var contentHTML = "";
    arr.forEach((item, index) => {
        var elementHTML = `
        <button class="color-button ${item}" onclick="changeColor('${item}', ${index})"></button>`
        return contentHTML += elementHTML;
    });
    document.getElementById('colorContainer').innerHTML = contentHTML;
}
loadColor(colorList);

var btnColor = document.querySelectorAll('.color-button');

changeColor = (arrEle, index) => {

    for (var j = 0; j < btnColor.length; j++) {
        btnColor[j].classList.remove("active");
    }
    btnColor[index].classList.add("active");
    var house = document.getElementById('house');
    house.classList = "house " + arrEle;

}

// =============================
